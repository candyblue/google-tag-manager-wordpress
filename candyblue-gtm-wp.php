<?php
/**
 * Plugin Name: cndybl-cookies
 * Plugin URI: https://gitlab.com/candyblue/google-tag-manager-wordpress
 * Description: With this plugin you can add Google Tag Manager to your site without loading tons of unnecessary source code.
 * Version: 1.0
 * Author: Lukas Becker
 * Author URI: https://candyblue.agency
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Include the Tag Manager Code, placed in head
function cndybl_tag_manager_head() {
  include_once( plugin_dir_path( __FILE__ ) . 'includes/tag_manager_head.php');
}
add_action('wp_head', 'cndybl_tag_manager_head');

// Include the Tag Manager Code, placed in body
function cndybl_tag_manager_body() {
  include_once( plugin_dir_path( __FILE__ ) . 'includes/tag_manager_body.php');
}
add_action('wp_body_open', 'cndybl_tag_manager_body');




function cndybl_register_settings() {
  add_option( 'cndybl_tag_manager_id', 'This is my option value.');
  register_setting( 'cndybl_options_group', 'cndybl_tag_manager_id', 'cndybl_callback' );
}
add_action( 'admin_init', 'cndybl_register_settings' );

function cndybl_register_options_page() {
  add_options_page('Google Tag Manager', 'Google Tag Manager', 'manage_options', 'cndybl', 'cndybl_options_page');
}
add_action('admin_menu', 'cndybl_register_options_page');

function cndybl_options_page()
{
?>
  <div>
  <?php screen_icon(); ?>
  <h2>Google Tag Manager</h2>
  <form method="post" action="options.php">
  <?php settings_fields( 'cndybl_options_group' ); ?>
  <h3>Google Tag Manager ID</h3>
  <p>Bitte gib Deine Google Tag Manager ID hier ein.</p>
  <table>
  <tr valign="top">
  <th scope="row"><label for="cndybl_tag_manager_id">Google Tag Manager ID</label></th>
  <td><input type="text" id="cndybl_tag_manager_id" name="cndybl_tag_manager_id" value="<?php echo get_option('cndybl_tag_manager_id'); ?>" /></td>
  </tr>
  </table>
  <?php submit_button(); ?>
  </form>
  </div>
  <?php
}
