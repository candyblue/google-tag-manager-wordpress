<?php
$cndybl_tag_manager_id = get_option( 'cndybl_tag_manager_id' );

if(get_option('cndybl_tag_manager_id')) {
?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $cndybl_tag_manager_id ?>"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php }; ?>
